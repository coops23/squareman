package com.studios.coops.squareman;

import android.graphics.Color;

import java.util.ArrayList;

public class SquareMan {

    final int SQUAREMAN_SIZE = 50;
    final int SQUAREMAN_COLOR = Color.RED;
    final double SQUAREMAN_VEL = 0.05;

    private int mX;
    private int mY;
    private int mWidth;
    private int mHeight;
    private int mColor;
    private double mSpeed;
    private double mDx, mDy;
    private double mDirectionX, mDirectionY;
    private ArrayList<Projectile> projectiles = new ArrayList<Projectile>();

    SquareMan(int x, int y)
    {
        mX = x;
        mY = y;
        mWidth = SQUAREMAN_SIZE;
        mHeight = SQUAREMAN_SIZE;
        mColor = SQUAREMAN_COLOR;
        mSpeed = (float) SQUAREMAN_VEL;
        mDx = 0;
        mDy = 0;
        mDirectionX = 0;
        mDirectionY = 1;
    }

    public void update(float deltaTime)
    {
        mX += (int)(mDx * mSpeed * (double)deltaTime);
        mY += (int) (mDy * mSpeed * (double) deltaTime);
        if ((mDx != 0) && (mDy != 0)) {
            mDirectionX = mDx;
            mDirectionY = mDy;
        }
    }

    public void shoot()
    {
        Projectile p = new Projectile(mX + (mWidth / 2), mY + (mHeight / 2), mDirectionX, mDirectionY);
        projectiles.add(p);
    }

    public int getX()
    {
        return mX;
    }

    public int getY()
    {
        return mY;
    }

    public int getWidth()
    {
        return mWidth;
    }

    public int getHeight()
    {
        return mHeight;
    }

    public int getColor()
    {
        return mColor;
    }

    public double getSpeed()
    {
        return mSpeed;
    }

    public ArrayList getProjectiles() { return projectiles; }

    public void setX(int x)
    {
        mX = x;
    }

    public void setY(int y)
    {
        mY = y;
    }

    public void setSpeed(double speed)
    {
        mSpeed = speed;
    }

    public void setMoving(int dx, int dy)
    {
        mDx = (double) dx;
        mDy = (double) dy;
    }
}
