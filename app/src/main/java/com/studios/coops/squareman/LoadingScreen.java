package com.studios.coops.squareman;

import com.kilobolt.framework.Game;
import com.kilobolt.framework.Graphics;
import com.kilobolt.framework.Graphics.ImageFormat;
import com.kilobolt.framework.Screen;

public class LoadingScreen extends Screen {
    public LoadingScreen(Game game) {
        super(game);
    }


    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newImage("IntroScreen.jpg", ImageFormat.RGB565);
        Assets.click = game.getAudio().createSound("click.ogg");

        game.setScreen(new MainMenuScreen(game));
    }


    @Override
    public void paint(float deltaTime) {
    }


    @Override
    public void pause() {
    }


    @Override
    public void resume() {

    }


    @Override
    public void dispose() {;
    }


    @Override
    public void backButton() {
    }
}
