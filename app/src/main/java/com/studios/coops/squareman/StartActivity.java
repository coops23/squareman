package com.studios.coops.squareman;

import android.app.Activity;
import android.os.Bundle;

import com.kilobolt.framework.Screen;
import com.kilobolt.framework.com.kilobolt.framework.implementation.AndroidGame;


public class StartActivity extends AndroidGame {

    @Override
    public Screen getInitScreen() {
        return new LoadingScreen(this);
    }

}
