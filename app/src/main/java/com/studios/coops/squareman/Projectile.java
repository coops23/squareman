package com.studios.coops.squareman;

import android.graphics.Rect;


public class Projectile {

    final int PROJECTILE_SPEED_DEFAULT = 3;

    private int x, y, speed, width, height;
    private boolean visible;
    private int dx, dy;

    private Rect r;

    public Projectile(int startX, int startY, double directionX, double directionY){
        double angle;

        x = startX;
        y = startY;
        speed = PROJECTILE_SPEED_DEFAULT;
        visible = true;
        width = height = 10;

        angle = Math.atan2(directionY, directionX);

        dx = (int) (Math.cos(angle) * speed);
        dy = (int) (Math.sin(angle) * speed);

        r = new Rect(x, y, width, height);
    }

    public void update(float deltaTime){
        x += (int)((double)(dx * speed) * (double)deltaTime);
        y += (int)((double)(dy * speed) * (double)deltaTime);
        r.set(x, y, x + width, y + height);
        if ((x > 800) || (x < 0) || (y < 0) || (y > 1180)){
            visible = false;
            r = null;
        }
        /*if (x < 800){
            checkCollision();
        }*/
    }

    private void checkCollision() {

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getSpeed() {
        return speed;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }


}
